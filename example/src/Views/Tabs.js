import React from "react";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

export default () => {
  return (
    <div className="nav-tabs-container hide-scroll">
      <Tabs defaultActiveKey="first" id="tabs">
        <Tab eventKey="first" title="First" />
        <Tab eventKey="second" title="Second" />
        <Tab eventKey="third" title="Third" />
        <Tab eventKey="fourth" title="Fourth" />
        <Tab eventKey="five" title="Five" />
        <Tab eventKey="six" title="Six" />
      </Tabs>
    </div>
  );
};
