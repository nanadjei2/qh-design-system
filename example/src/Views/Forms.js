import React from "react";
import { requirePhoneNumber, Field, Select, Date } from "qh-design-system";
import { Formik } from "formik";
import { object } from "yup";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";

const Forms = () => {
  return (
    <Formik
      initialValues={{
        hello: "",
        date: "",
        from: "",
        to: "",
        country: "",
        phone: "",
      }}
      validationSchema={object({
        phone: requirePhoneNumber("phone number", false),
      })}
    >
      {({
        handleSubmit,
        values,
        setFieldValue,
        setFieldError,
        setFieldTouched,
      }) => (
        <Form>
          <Form.Group>
            <Form.Label>Hello world</Form.Label>
            <Form.Control />
          </Form.Group>
          <Form.Group>
            <Form.Label>Hello world</Form.Label>
            <Form.Control
              disabled
              className="form-control-warning"
              placeholder="Hello world"
              value="Hello world"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Checkbox and Radio</Form.Label>
            <Card>
              <Card.Body className="p-4">
                <Form.Check
                  custom
                  id="hello"
                  type="checkbox"
                  className="mb-4"
                  label={<span>Hello world</span>}
                />
                <Form.Check
                  custom
                  type="radio"
                  id="hello-1"
                  className="mb-4"
                  label={<span>Hello world</span>}
                />
              </Card.Body>
            </Card>
          </Form.Group>

          <Form.Group>
            <Form.Label>Switch</Form.Label>
            <Card>
              <Card.Body className="p-4">
                <Form.Check
                  custom
                  className="check-red"
                  type="switch"
                  id="switch-1"
                />
              </Card.Body>
            </Card>
          </Form.Group>

          <Field
            name="hello"
            component={Select}
            placeholder="Hello"
            value={values.hello}
            label="Select"
            options={[
              {
                label: "Hello world",
                value: "hello",
                isUrl: true,
                avatar: "https://via.placeholder.com/150",
              },
            ]}
            onChange={({ value }) => setFieldValue("hello", value)}
          />

          <Field
            isDisabled
            name="hello"
            value="hello"
            component={Select}
            placeholder="Hello"
            label="Select Disabled"
            options={[
              {
                label: "Hello world",
                value: "hello",
              },
            ]}
          />

          <Date.Picker
            name="date"
            label="Select Date"
            value={values.date}
            maxDate={new window.Date()}
            {...{ setFieldValue }}
          />
          <Date.Range
            label="Date uploaded"
            names={["from", "to"]}
            values={[values.from, values.to]}
            {...{ setFieldValue }}
          />

          {/* <Field
            name="country"
            label="Country"
            placeholder=""
            component={Countries}
            value={values.country}
            onChange={({ value }) => setFieldValue("country", value)}
          />

          <PhoneInput
            name="phone"
            value={values.phone}
            label="Phone number"
            // onlyCountries={["GH", "NG"]}
            {...{ setFieldTouched, setFieldValue }}
          /> */}
        </Form>
      )}
    </Formik>
  );
};

export default Forms;
