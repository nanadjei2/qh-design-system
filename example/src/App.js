import React from "react";
import {
  BrowserRouter,
  NavLink,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import styled from "styled-components";

import Alerts from "./Views/Alerts";
import Progress from "./Views/Progress";
import Icons from "./Views/Icons";
import Buttons from "./Views/Buttons";
import Forms from "./Views/Forms";
import Tables from "./Views/Tables";
import Skeletons from "./Views/Skeletons";
import Labels from "./Views/Labels";
// import Search from "./Views/Search";
import Confirm from "./Views/Confirm";
import Dropdowns from "./Views/Dropdowns";
// import CheckedTab from "./Views/CheckedTab";
// import Layouts from "./Views/Layouts";

const App = () => {
  return (
    <BrowserRouter>
      <Wrapper>
        <SideNav>
          <h3>Icons</h3>
          <NavLink to="/icons">Icons</NavLink>
          <h3>Components</h3>
          <NavLink to="/alerts">Alerts</NavLink>
          <NavLink to="/buttons">Buttons</NavLink>
          <NavLink to="/confirm">Confirm</NavLink>
          <NavLink to="/forms">Forms</NavLink>
          <NavLink to="/tables">Tables</NavLink>
          <NavLink to="/skeletons">Skeletons</NavLink>
          <NavLink to="/dropdowns">Dropdowns</NavLink>
          <NavLink to="/progress">Progress</NavLink>
          <NavLink to="/labels">Labels</NavLink>

          {/* <NavLink to="/searcher">Searcher</NavLink> */}
          {/* <NavLink to="/checkedtab">Checked Tab</NavLink> */}
          {/* <NavLink to="/layouts">Layouts</NavLink> */}
        </SideNav>
        <Content>
          <TopNav className="x-padding"></TopNav>
          <Body className="x-padding">
            <Switch>
              <Route exact path="/alerts" component={Alerts} />
              <Route exact path="/progress" component={Progress} />
              <Route exact path="/icons" component={Icons} />
              <Route exact path="/buttons" component={Buttons} />
              <Route exact path="/forms" component={Forms} />
              <Route exact path="/confirm" component={Confirm} />
              <Route exact path="/tables" component={Tables} />
              <Route exact path="/skeletons" component={Skeletons} />
              <Route exact path="/dropdowns" component={Dropdowns} />
              <Route exact path="/labels" component={Labels} />
              {/* <Route exact path="/checkedtab" component={CheckedTab} /> */}
              {/* <Route exact path="/searcher" component={Search} /> */}
              {/* <Route exact path="/layouts" component={Layouts} /> */}
              <Redirect from="/" to="/icons" />
            </Switch>
          </Body>
        </Content>
      </Wrapper>
    </BrowserRouter>
  );
};

/**
 * styles
 */

const Body = styled.div`
  height: 100%;
  overflow: auto;
  padding-top: 16px;
  padding-bottom: 16px;

  @media (min-width: 768px) {
    padding-top: 24px;
    padding-bottom: 24px;
  }
`;

const TopNav = styled.div`
  display: flex;
  align-items: center;
`;

const Content = styled.div`
  height: 100%;
  display: grid;
  overflow: hidden;
  grid-template-rows: 76px 1fr;
`;

const SideNav = styled.div`
  width: 256px;
  height: 100%;
  overflow: auto;
  padding: 76px 24px;
  background-color: var(--desert-storm);

  a {
    padding: 16px;
    display: block;
    font-weight: 500;
    border-radius: 8px;
    margin-bottom: 24px;
    text-decoration: none;
    color: var(--black-pearl);

    &.active {
      background-color: #fff;
      color: var(--radiant-blue);
    }

    span {
      font-size: 16px;
      line-height: 24px;
      margin-left: 16px;
    }

    &:last-child {
      margin-bottom: 0px;
    }
  }
`;

const Wrapper = styled.div`
  height: 100%;
  display: grid;
  overflow: hidden;
  grid-template-columns: 256px 1fr;
`;

export default App;
