import React, { useEffect } from "react";
import { ToastContainer } from "react-toastify";
import { SWRConfig } from "swr";

import ErrorBoundary from "../Utils/ErrorBoundary";
import Http from "../Utils/Http";

const AppProvider = ({ children }) => {
  /**
   * effect
   */
  useEffect(() => {
    const events = ["touchstart", "scroll"];

    events.forEach((event) =>
      document.addEventListener(event, () => {}, {
        passive: true,
      }),
    );

    return () => {
      events.forEach((event) =>
        document.removeEventListener(event, () => {}, {
          passive: false,
        }),
      );
    };
  }, []);

  return (
    <ErrorBoundary>
      <SWRConfig
        value={{
          fetcher: (url) => Http.get(url).then((data) => data),
          dedupingInterval: 1000 * 60 * 15,
          shouldRetryOnError: false,
          errorRetryInterval: 0,
          errorRetryCount: 2,
        }}
      >
        {children}
      </SWRConfig>
      <ToastContainer />
    </ErrorBoundary>
  );
};

export default AppProvider;
