import React from "react";
import styled from "styled-components";

const AbbrBubble = ({ children, ...props }) => {
  return (
    <Wrapper {...props}>
      <div className="abbr text-body -medium">{children}</div>
      <svg
        fill="none"
        width="100%"
        height="100%"
        viewBox="0 0 41 40"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle cx="20.5" cy="20" r="20" fill="var(--radiant-blue)" />
        <mask
          id="mask0"
          mask-type="alpha"
          maskUnits="userSpaceOnUse"
          x="0"
          y="0"
          width="41"
          height="40"
        >
          <circle cx="20.5" cy="20" r="20" fill="white" />
        </mask>
        <g mask="url(#mask0)">
          <ellipse
            cx="27.0217"
            cy="28.6643"
            rx="16.5217"
            ry="16.5217"
            fill="url(#paint0_linear)"
          />
        </g>
        <defs>
          <linearGradient
            id="paint0_linear"
            x1="19.7935"
            y1="12.1426"
            x2="41.4783"
            y2="69.4524"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="var(--radiant-blue)" />
            <stop offset="1" stopColor="#EBF3FE" />
          </linearGradient>
        </defs>
      </svg>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
  width: ${({ size }) => size || "40px"};
  height: ${({ size }) => size || "40px"};

  .abbr {
    color: #fff;
    position: absolute;
  }
`;

export default AbbrBubble;
