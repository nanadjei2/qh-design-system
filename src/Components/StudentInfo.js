import React, { Fragment } from "reactn";
import startCase from "lodash/startCase";
import styled from "styled-components";
import moment from "moment";
import useSWR from "swr";
import Card from "react-bootstrap/Card";

import DataItem from "./DataItem";
import Spinner from "./Spinner";
import Upload from "./Upload";
import Grid from "./Grid";

const StudentInfo = ({ id }) => {
  /**
   * api
   */
  const { data, error } = useSWR(
    `/svc/enrolment/api/personnel-enrolment/${id}`,
  );

  const {
    personnel_service_information,
    personnel_application_info,
    personnel_identification,
    personnel_preference,
  } = data || {};

  /**
   * conditional API
   */
  const { data: regions } = useSWR("/svc/enrolment/api/region");
  const { data: districts } = useSWR("/svc/enrolment/api/district");
  const { data: studyTypes } = useSWR("/svc/enrolment/api/study-leave");

  return (
    <Fragment>
      {!data && !error && <Spinner />}
      {data && (
        <Wrapper className="py-2 py-md-6">
          <CardGrid>
            <Card>
              <Card.Body>
                <h3>Applicant information</h3>
                <ContentGrid lg>
                  <div>
                    <p className="sub-header">Personal details</p>

                    <div>
                      <Content className="mb-6">
                        <Grid className="order-2" gap="24px">
                          <DataItem header="Full name">
                            {data?.other_name + " " + data?.surname}
                          </DataItem>
                          <DataItem header="Pincode">
                            {data?.pin_number}
                          </DataItem>
                          <DataItem header="Date of birth">
                            {moment(data?.date_of_birth).format(
                              "MMMM DD, YYYY",
                            )}
                          </DataItem>
                        </Grid>
                        <ImgWrapper>
                          <img
                            src={personnel_application_info?.passport_url}
                            alt=""
                          />
                        </ImgWrapper>
                      </Content>
                      <Content>
                        <DataItem header="Email address">
                          {personnel_application_info?.email || "--"}
                        </DataItem>
                        <DataItem header="Marital status">
                          {startCase(
                            personnel_application_info?.marital_status || "",
                          ) || "--"}
                        </DataItem>
                        <DataItem header="Residential address">
                          {personnel_application_info?.residential_address ||
                            "--"}
                        </DataItem>
                        <DataItem header="Mobile number">
                          {personnel_application_info?.phone_number || "--"}
                        </DataItem>
                      </Content>
                    </div>
                  </div>

                  {personnel_identification && (
                    <div>
                      <p className="sub-header">Means of identification</p>

                      <Grid gap="24px">
                        <ImgWrapper lg>
                          <img
                            src={`data:image/png;base64, ${personnel_identification?.picture}`}
                            alt=""
                          />
                        </ImgWrapper>
                        <DataItem header="ID type">
                          {personnel_identification?.national_id_type?.name}
                        </DataItem>
                      </Grid>
                    </div>
                  )}
                </ContentGrid>
              </Card.Body>
              <Card.Body>
                <ContentGrid>
                  <div>
                    <p className="sub-header">Guardian’s details</p>

                    <Content>
                      <DataItem header="Guardian’s name">
                        {personnel_application_info?.guardian_name || "--"}
                      </DataItem>
                      <DataItem header="Email address">
                        {personnel_application_info?.guardian_email || "--"}
                      </DataItem>
                      <DataItem header="Mobile number">
                        {personnel_application_info?.guardian_phone_number ||
                          "--"}
                      </DataItem>
                    </Content>
                  </div>
                  <div>
                    <p className="sub-header">Next of kin’s details</p>

                    <Content>
                      <DataItem header="Next of kin’s name">
                        {personnel_application_info?.next_of_kin_name || "--"}
                      </DataItem>
                      <DataItem header="Relationship">
                        {personnel_application_info?.relation_to_next_of_kin ||
                          "--"}
                      </DataItem>
                      <DataItem header="Mobile number">
                        {personnel_application_info?.next_of_kin_phone_number ||
                          "--"}
                      </DataItem>
                    </Content>
                  </div>
                </ContentGrid>
              </Card.Body>
            </Card>

            {personnel_service_information && (
              <Card>
                <Card.Body>
                  <h3>Service information</h3>
                  <ContentGrid>
                    <Grid
                      gap="24px"
                      className="athens-grey-border border-bottom pb-6 grid-item order-1"
                    >
                      <DataItem header="Are you currently employed?">
                        {personnel_service_information?.currently_employed
                          ? "Yes"
                          : "No"}
                      </DataItem>

                      {personnel_service_information?.currently_employed && (
                        <Fragment>
                          <DataItem header="Employment / Study leave type">
                            {studyTypes?.find(
                              (s) =>
                                s.id ===
                                personnel_service_information?.study_leave_type_id,
                            )?.name || "--"}
                          </DataItem>
                          <DataItem header="Proof of employment">
                            <Upload
                              value={{
                                name:
                                  personnel_service_information?.study_leave_document,
                              }}
                            />
                          </DataItem>
                          <DataItem header="Organization region">
                            {regions?.find(
                              (r) =>
                                r.id ===
                                personnel_service_information?.study_leave_region_id,
                            )?.name || "--"}
                          </DataItem>
                          <DataItem header="Organization district">
                            {districts?.find(
                              (d) =>
                                d.id ===
                                personnel_service_information?.study_leave_district_id,
                            )?.name || "--"}
                          </DataItem>
                          {personnel_service_information?.study_leave_sponsor_name && (
                            <DataItem header="Name of organization">
                              {personnel_service_information?.study_leave_sponsor_name ||
                                "--"}
                            </DataItem>
                          )}
                          {personnel_service_information?.study_leave_sponsor_phone && (
                            <DataItem header="Name of organization">
                              {personnel_service_information?.study_leave_sponsor_phone ||
                                "--"}
                            </DataItem>
                          )}
                        </Fragment>
                      )}
                    </Grid>

                    <Grid
                      gap="24px"
                      className="border-bottom athens-grey-border pb-6 grid-item order-2"
                    >
                      <DataItem header="Do you have any health conditions?">
                        {personnel_service_information?.has_health_condition
                          ? "Yes"
                          : "No"}
                      </DataItem>

                      {personnel_service_information?.has_health_condition && (
                        <Fragment>
                          <DataItem header="Health condition type">
                            {personnel_service_information?.health_condition
                              ?.name || "--"}
                          </DataItem>
                          <DataItem header="Health condition document">
                            <Upload
                              value={{
                                name:
                                  personnel_service_information?.health_condition_document,
                              }}
                            />
                          </DataItem>
                          <DataItem header="Health condition details">
                            {personnel_service_information?.health_condition_details ||
                              "--"}
                          </DataItem>
                        </Fragment>
                      )}
                    </Grid>
                    <Grid gap="24px" className="order-3 order-md-4">
                      <DataItem header="Have you done your national service before?">
                        {personnel_service_information?.undergone_service_before
                          ? "Yes"
                          : "No"}
                      </DataItem>
                      {personnel_service_information?.undergone_service_before && (
                        <DataItem header="Organization you served with">
                          {personnel_service_information?.name_of_organization ||
                            "--"}
                        </DataItem>
                      )}
                    </Grid>
                    <DataItem
                      header="Do you have military training?"
                      className="order-4 order-md-3"
                    >
                      {personnel_service_information?.attended_military_training
                        ? "Yes"
                        : "No"}
                    </DataItem>
                  </ContentGrid>
                </Card.Body>
                <Card.Body>
                  <Grid
                    gap="24px"
                    sm="repeat(2, 1fr)"
                    md="138px 130px 110px auto"
                    style={{ maxWidth: 640 }}
                  >
                    <DataItem header="Trouser length" className="order-md-1">
                      {personnel_service_information?.trouser_length || "--"}
                    </DataItem>
                    <DataItem header="Shirt neck" className="order-md-3">
                      {personnel_service_information?.shirt_neck || "--"}
                    </DataItem>
                    <DataItem header="Trouser waist" className="order-md-2">
                      {personnel_service_information?.trouser_waist || "--"}
                    </DataItem>
                    <DataItem header="Shirt size" className="order-md-4">
                      {personnel_service_information?.shirt_size || "--"}
                    </DataItem>
                  </Grid>
                </Card.Body>
              </Card>
            )}

            {personnel_preference && (
              <Card>
                <Card.Body>
                  <h3>Preference</h3>

                  <ContentGrid>
                    <div>
                      <p className="sub-header">
                        Select in order of priority the regions you wish to be
                        considered for the National Service Posting
                      </p>

                      <div className="items">
                        <div className="item">
                          <span>Region I</span>
                          <span>
                            {personnel_preference?.region_one?.name || "--"}
                          </span>
                        </div>
                        <div className="item">
                          <span>Region II</span>
                          <span>
                            {personnel_preference?.region_two?.name || "--"}
                          </span>
                        </div>
                        <div className="item">
                          <span>Region III</span>
                          <span>
                            {personnel_preference?.region_three?.name || "--"}
                          </span>
                        </div>
                      </div>
                    </div>
                  </ContentGrid>
                </Card.Body>
                <Card.Body>
                  <ContentGrid>
                    <div>
                      <p className="sub-header">Local languages</p>
                      <Grid gap="24px">
                        <DataItem header="How many of these can you read?">
                          {personnel_preference?.can_read_local_languages
                            ?.map((i) => startCase(i))
                            ?.join(", ") || "--"}
                        </DataItem>
                        <DataItem header="How many of these can you speak?">
                          {personnel_preference?.can_speak_local_languages
                            ?.map((i) => startCase(i))
                            ?.join(", ") || "--"}
                        </DataItem>
                        <DataItem header="How many of these can you write?">
                          {personnel_preference?.can_write_local_languages
                            ?.map((i) => startCase(i))
                            ?.join(", ") || "--"}
                        </DataItem>
                      </Grid>
                    </div>
                    <div>
                      <p className="sub-header">International languages</p>
                      <Grid gap="24px">
                        <DataItem header="How many of these can you read?">
                          {personnel_preference?.can_read_international_languages
                            ?.map((i) => startCase(i))
                            ?.join(", ") || "--"}
                        </DataItem>
                        <DataItem header="How many of these can you speak?">
                          {personnel_preference?.can_speak_international_languages
                            ?.map((i) => startCase(i))
                            ?.join(", ") || "--"}
                        </DataItem>
                        <DataItem header="How many of these can you write?">
                          {personnel_preference?.can_write_international_languages
                            ?.map((i) => startCase(i))
                            ?.join(", ") || "--"}
                        </DataItem>
                      </Grid>
                    </div>
                  </ContentGrid>
                </Card.Body>
                <Card.Body>
                  <p className="sub-header">
                    Industrial Preference - Service option
                  </p>

                  <ContentGrid>
                    <div className="items -lg">
                      <div className="item">
                        <span>Service option I</span>
                        <span>
                          {personnel_preference?.service_option_one?.name ||
                            "--"}
                        </span>
                      </div>
                      <div className="item">
                        <span>Service option II</span>
                        <span>
                          {personnel_preference?.service_option_two?.name ||
                            "--"}
                        </span>
                      </div>
                      {personnel_preference?.service_option_three && (
                        <div className="item">
                          <span>Service option III</span>
                          <span>
                            {personnel_preference?.service_option_three?.name ||
                              "--"}
                          </span>
                        </div>
                      )}
                    </div>
                  </ContentGrid>
                </Card.Body>
              </Card>
            )}
          </CardGrid>
        </Wrapper>
      )}
    </Fragment>
  );
};

/**
 * style
 */
const Wrapper = styled.div`
  .card {
    &-body {
      padding: 24px 16px;
      h3 {
        margin-bottom: 24px;
      }

      .sub-header {
        font-size: 16px;
        font-weight: 500;
        line-height: 24px;
        margin-bottom: 16px;
      }
    }
  }

  @media (min-width: 768px) {
    .card {
      &-body {
        padding: 40px;
      }
    }
  }
`;

const CardGrid = styled.div`
  display: grid;
  gap: 24px;

  @media (min-width: 768px) {
    gap: 40px;
  }
`;

const ContentGrid = styled.div`
  display: grid;
  row-gap: ${({ lg }) => (lg ? "32px" : "24px")};

  .grid-item {
    grid-auto-rows: minmax(min-content, max-content);
  }

  .items {
    display: grid;
    gap: 24px;

    .item {
      gap: 40px;
      display: grid;
      grid-template-columns: 60px auto;

      span:first-child {
        font-size: 14px;
        line-height: 22px;
        white-space: nowrap;
      }

      span:last-child {
        font-size: 16px;
        line-height: 24px;
        color: var(--black-pearl);
      }
    }

    &.-lg {
      .item {
        grid-template-columns: 112px auto;
      }
    }
  }

  ${({ lg }) =>
    lg
      ? `
      @media (min-width: 992px) {
        grid-template-columns: 45% 45%;
        column-gap: 10%;
      }`
      : `
      @media (min-width: 768px) {
        grid-template-columns: 45% 45%;
        column-gap: 10%;
      }
  `}
`;

const Content = styled.div`
  display: grid;
  row-gap: 24px;

  @media (min-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
    column-gap: 24px;
  }
`;

const ImgWrapper = styled.div`
  padding: 1px;
  height: 200px;
  border-radius: 8px;
  background-color: var(--desert-storm);
  width: ${({ lg }) => (lg ? `310px` : `150px`)};

  img {
    object-fit: contain;
    display: block;
    height: 100%;
    width: 100%;
  }
`;

export default StudentInfo;
