import React, { useState } from "react";
import { getCountries, formatPhoneNumberIntl } from "react-phone-number-input";
import { ErrorMessage, FastField } from "formik";
import countryList from "react-select-country-list";
import PropTypes from "prop-types";
import Dropdown from "react-bootstrap/Dropdown";
import styled from "styled-components";
import flags from "emoji-flags";
import Phone from "react-phone-number-input/input";
import Form from "react-bootstrap/Form";

import ChevronDown from "../Icons/ChevronDown";

/**
 * props definition
 */
const propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  setFieldValue: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  onlyCountries: PropTypes.array,
};

const defaultProps = {
  placeholder: "",
  label: "Phone number",
  setFieldValue: () => {},
  setFieldTouched: () => {},
};

const PhoneInput = ({
  name,
  value,
  label,
  className,
  placeholder,
  onlyCountries,
  setFieldValue,
}) => {
  /**
   * states
   */
  const [country = "GH", setCountry] = useState("GH");

  /**
   * functions
   */
  const handleCountries = () => {
    const selectedCountries = onlyCountries || getCountries();

    return selectedCountries
      .map((country) => {
        if (!country) return false;

        const countryName = countryList().getLabel(country.toUpperCase());

        if (countryName) {
          return {
            label: flags.countryCode(country.toUpperCase())?.emoji,
            value: countryName,
            country,
          };
        }

        return {};
      })
      .filter((e) => e)
      .sort((a, b) => {
        const nameA = a?.value?.toUpperCase();
        const nameB = b?.value?.toUpperCase();

        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      });
  };

  return (
    <Form.Group className={className} controlId={name}>
      <Form.Label>{label}</Form.Label>
      <Wrapper className="input-container">
        <Dropdown>
          <Toggle>
            <span role="img" aria-label={country}>
              {flags.countryCode(country)?.emoji}
            </span>
            <ChevronDown />
          </Toggle>
          <Menu>
            {handleCountries().map(({ label, value, ...others }, key) => {
              if (!label || !value) return false;

              return (
                <Dropdown.Item
                  key={key}
                  className={
                    country?.toLowerCase() === others?.country?.toLowerCase()
                      ? "active"
                      : ""
                  }
                  onClick={() => setCountry(others.country)}
                >
                  <span role="img" aria-label={others.country}>
                    {label}
                  </span>
                  <span className="ml-1">{value}</span>
                </Dropdown.Item>
              );
            })}
          </Menu>
        </Dropdown>
        <FastField
          international
          country={country}
          component={Phone}
          className="form-control pl-0"
          onChange={(value) =>
            setFieldValue(
              name,
              formatPhoneNumberIntl(value)?.replace(/\s/g, ""),
            )
          }
          {...{ value, placeholder }}
        />
      </Wrapper>
      <ErrorMessage name={name}>
        {(msg) => <p className="text-sub cinnabar-text">{msg}</p>}
      </ErrorMessage>
    </Form.Group>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  padding: 0px;
  display: flex;
  align-items: center;
`;

const Toggle = styled(Dropdown.Toggle)`
  background-color: transparent !important;
  width: calc(24px + 24px + 2rem);
  justify-content: center;
  align-items: center;
  flex-wrap: nowrap;
  display: flex;
  padding: 0px;

  span {
    font-size: 24px;
  }
`;

const Menu = styled(Dropdown.Menu)`
  .dropdown-item {
    padding-left: 1rem;
    padding-right: 1rem;
  }
`;

PhoneInput.propTypes = propTypes;
PhoneInput.defaultProps = defaultProps;

export default React.memo(PhoneInput);
