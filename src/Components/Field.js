import React from "react";
import Form from "react-bootstrap/Form";
import PropTypes from "prop-types";

import { ErrorMessage, Field as Fld } from "formik";

import ErrorBoundary from "../Utils/ErrorBoundary";

/**
 * props definition
 */
const propTypes = {
  containerProps: PropTypes.object,
  startadornment: PropTypes.object,
  endadornment: PropTypes.object,
  useComponent: PropTypes.bool,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.any,
  label: PropTypes.string,
  value: PropTypes.any.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  as: PropTypes.string,
};

const defaultProps = {
  useComponent: true,
  type: "text",
};

/**
 *
 * @param containerProps form-group props
 * @param startadornment put element infront of input box
 * @param endadornment put element after input box
 * @param useComponent determine whether to use formik field or component
 * @param placeholder placeholder for input box
 * @param className input box css className
 * @param label input box label
 * @param value input box value | required
 * @param name input box name | required
 * @param type input box type
 * @param as input box can switch to different type | select or textbox
 */
const Field = ({
  containerProps,
  startadornment,
  endadornment,
  useComponent,
  placeholder,
  className,
  children,
  label,
  value,
  name,
  type,
  as,
  ...props
}) => {
  const Render = props.component && useComponent ? props.component : Fld;

  return (
    <ErrorBoundary>
      <Form.Group controlId={name} {...containerProps}>
        {label && <Form.Label>{label}</Form.Label>}
        <div
          className={`position-relative d-flex align-items-center ${
            startadornment || endadornment ? "input-container" : ""
          }`}
        >
          {startadornment && (
            <span
              className={`start-adornment ${
                startadornment?.props?.className || ""
              }`}
              {...startadornment?.props}
            >
              {startadornment.children}
            </span>
          )}
          <Render
            className={`form-control ${className || ""} ${
              props.component && useComponent ? "px-0 --border-none" : ""
            } ${startadornment || endadornment ? "--adorned" : ""}`}
            placeholder={placeholder}
            title={placeholder}
            value={value}
            name={name}
            {...(children && { children: children })}
            {...(type && { type: type })}
            {...(as && { as: as })}
            {...props}
          />

          {endadornment && (
            <span
              className={`end-adornment ${endadornment?.props?.className}`}
              {...endadornment?.props}
            >
              {endadornment.children}
            </span>
          )}
        </div>
        <ErrorMessage
          name={name}
          component="p"
          className="text-sub cinnabar-text ml-4 mb-0"
        />
      </Form.Group>
    </ErrorBoundary>
  );
};

Field.propTypes = propTypes;
Field.defaultProps = defaultProps;

export default Field;
