import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import Container from "react-bootstrap/Container";
import PropTypes from "prop-types";
import styled from "styled-components";
import Fade from "react-fade-in";

import useWidth from "../Hooks/useWidth";
import CheckedTab from "./CheckedTab";
import Layout from "./Layout";
import Back from "./Back";

/**
 * props definition
 */
const propTypes = {
  show: PropTypes.shape({
    tabsOnMobile: PropTypes.bool,
    back: PropTypes.bool,
  }),
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      slug: PropTypes.string,
      component: PropTypes.any,
    }),
  ).isRequired,
  mainUrl: PropTypes.string,
  previousUrl: PropTypes.string,
  header: PropTypes.string.isRequired,
  description: PropTypes.string,
  activeKey: PropTypes.number,
  children: PropTypes.any,
  childProps: PropTypes.object,
  home: PropTypes.string,
};

const defaultProps = {
  childProps: {},
  show: { tabsOnMobile: true, back: true },
  home: "/",
};

/**
 *
 * @param {*} show an object of items to show or hide
 * @param {*} tabs an array of items to display on tabs | title, slug, componenet
 * @param {*} header header of layout
 * @param {*} mainUrl url to mutate, url must have :tab
 * @param {*} children if components are not in tabs, default to children
 * @param {*} childProps props to pass to each components in tabs
 * @param {*} previousUrl url to navigate to on close
 * @param {*} description caption for the layout
 * @param {*} activeKey manually set active key outside component
 */
const TabLayout = ({
  home,
  show,
  tabs,
  header,
  mainUrl,
  children,
  childProps,
  previousUrl,
  description,
  activeKey: keyActive,
}) => {
  /**
   * state
   */
  const [activeTab, setTab] = useState(null);
  const [activeKey, setKey] = useState(0);

  /**
   * variables
   */
  const url = mainUrl?.replace(":tab", "");
  const { tab } = useParams();
  const history = useHistory();
  const width = useWidth();

  /**
   * functions
   */
  const handleBack = () => {
    if (activeTab) {
      const index = tabs.findIndex(({ slug }) => slug === activeTab.slug);

      if (index >= 0) {
        if (index === 0) {
          history.push(previousUrl);
          return false;
        }

        history.push(mainUrl.replace(":tab", tabs[index - 1].slug));
        return false;
      }
    }
  };

  /**
   * effect
   */
  useEffect(() => {
    if (tab && tabs.map(({ slug }) => slug).includes(tab)) {
      const index = tabs.findIndex(({ slug }) => slug === tab);
      setTab(tabs[index]);
      setKey(index);
    }
  }, [tab, tabs]);

  useEffect(() => {
    if (keyActive >= 0) {
      setKey(keyActive);
    }
  }, [keyActive]);

  return (
    <Layout
      header={header}
      back={{
        type: "arrow",
        link: () => handleBack(),
      }}
      home={home}
    >
      <Fade>
        <Wrapper className="py-2 py-md-6">
          <Sidenav>
            <div className="sidenav">
              {previousUrl && (
                <Back
                  icon="close"
                  className="mb-4"
                  onClick={() => history.push(previousUrl)}
                />
              )}
              <h1 className="mb-0 d-md-none d-lg-block">{header}</h1>
              {description && (
                <p className="text-body mt-4 d-md-none d-lg-block">
                  {description}
                </p>
              )}
              {(show?.tabsOnMobile ? true : width >= 768) && (
                <CheckedTab
                  {...{ tabs, activeKey }}
                  className="mt-4 my-md-10 py-md-9 py-lg-0 tabs"
                />
              )}
            </div>
          </Sidenav>
          <div
            className={"content-container" + activeKey === 0 ? "py-lg-12" : ""}
          >
            {activeKey > 0 && show?.back && (
              <Fade>
                <Back
                  icon="chevron"
                  className="mb-4 d-none d-md-grid mb-6"
                  onClick={() => handleBack()}
                >
                  Previous step
                </Back>
              </Fade>
            )}
            <h1 className="mb-4 d-none d-md-block d-lg-none">{header}</h1>
            {description && (
              <p className="mb-8 text-body d-none d-md-block d-lg-none">
                {description}
              </p>
            )}
            {activeTab?.component ? (
              <activeTab.component
                {...{
                  history,
                  url: `${url}${tabs[activeKey + 1]?.slug}`,
                  ...childProps,
                }}
              />
            ) : (
              children
            )}
          </div>
        </Wrapper>
      </Fade>
    </Layout>
  );
};

/**
 * styles
 */
const Wrapper = styled(Container)`
  display: grid;
  gap: 24px;

  @media (min-width: 768px) {
    grid-template-columns: 208px auto;
    gap: 48px;
  }

  @media (min-width: 992px) {
    grid-template-columns: 460px auto;
  }

  @media (min-width: 1200px) {
    gap: 132px;
  }
`;

const Sidenav = styled.div`
  .sidenav {
    position: sticky;
    top: 24px;
  }
`;

TabLayout.propTypes = propTypes;
TabLayout.defaultProps = defaultProps;

export default TabLayout;
