import styled from "styled-components";
import PropTypes from "prop-types";

const propTypes = {
  gap: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  row: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  column: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

  gapMd: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  rowMd: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  columnMd: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

  gapLg: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  rowLg: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  columnLg: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

  gapXl: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  rowXl: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  columnXl: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

  sm: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  md: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  lg: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  xl: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  flow: PropTypes.string,
};

const defaultProps = {
  gap: "32px",
  flow: "column",
};

const template = "minmax(0, 1fr)";

const Grid = styled.div`
  width: 100%;
  display: grid;
  row-gap: ${({ row }) => row};
  grid-gap: ${({ gap }) => gap};
  column-gap: ${({ column }) => column};
  grid-template-columns: ${({ sm }) => sm || template};

  @media (min-width: 768px) {
    row-gap: ${({ rowMd }) => rowMd};
    grid-gap: ${({ gapMd }) => gapMd};
    column-gap: ${({ columnMd }) => columnMd};
    grid-template-columns: ${({ sm, md }) => md || sm || template};
  }

  @media (min-width: 992px) {
    row-gap: ${({ rowLg }) => rowLg};
    grid-gap: ${({ gapLg }) => gapLg};
    column-gap: ${({ columnLg }) => columnLg};
    grid-template-columns: ${({ sm, md, lg }) => lg || md || sm || template};
  }

  @media (min-width: 1200px) {
    grid-template-columns: ${({ sm, md, lg, xl }) =>
      xl || lg || md || sm || template};
    grid-gap: ${({ gapXl }) => gapXl};
    row-gap: ${({ rowXl }) => rowXl};
    column-gap: ${({ columnXl }) => columnXl};
  }
`;

Grid.propTypes = propTypes;
Grid.defaultProps = defaultProps;

export default Grid;
