import Cookie from "js-cookie";

const cookie = () => {
  /**
   * use to set cookie
   * @param {*} key
   * @param {*} value
   */
  const setCookie = (key, value) => {
    return Cookie.set(key, value, {
      path: "/",
      expires: 60,
      domain: process.env.REACT_APP_PARENT_DOMAIN,
    });
  };

  /**
   * use to get cookie
   * @param {*} key
   */
  const getCookie = (key) => Cookie.get(key);

  /**
   * use to delete cookie
   * @param {*} key
   */
  const deleteCookie = (key) =>
    Cookie.remove(key, {
      path: "/",
      domain: process.env.REACT_APP_PARENT_DOMAIN,
    });

  return { setCookie, getCookie, deleteCookie };
};

export default cookie;
