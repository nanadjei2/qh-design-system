import React, { setGlobal } from "reactn";
import { toast } from "react-toastify";

import axios from "axios";

import cookie from "../Hooks/cookie";
import Notify from "../Components/Notify";

// variables
const rootState = {
  isAuthenticated: false,
  user: null,
};

export const Http = axios.create({
  baseURL: process.env.REACT_APP_API_BASEURL,
  timeout: 45000,
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

Http.interceptors.request.use((config) => {
  const token = cookie().getCookie("token");
  const userType = cookie().getCookie("userType");

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  if (userType) {
    config.headers.common["X-User-Type"] = userType;
  }

  return config;
});

Http.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error.response?.status) {
      if (error.response.status === 401) {
        cookie().deleteCookie("token");
        setGlobal(rootState);
      }

      if (error.response.status === 500) {
        toast(<Notify body="A server error occured" type="error" />);
      }
    }

    return Promise.reject(error);
  },
);

export default Http;
