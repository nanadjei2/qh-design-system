import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Photo = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M16 4H8C6.89543 4 6 4.89543 6 6V18C6 18.7403 6.4022 19.3866 7 19.7324V19C7 16.2386 9.23858 14 12 14C14.7614 14 17 16.2386 17 19V19.7324C17.5978 19.3866 18 18.7403 18 18V6C18 4.89543 17.1046 4 16 4ZM15 20V19C15 17.3431 13.6569 16 12 16C10.3431 16 9 17.3431 9 19V20H15ZM8 2C5.79086 2 4 3.79086 4 6V18C4 20.2091 5.79086 22 8 22H16C18.2091 22 20 20.2091 20 18V6C20 3.79086 18.2091 2 16 2H8ZM12 10C12.5523 10 13 9.55228 13 9C13 8.44772 12.5523 8 12 8C11.4477 8 11 8.44772 11 9C11 9.55228 11.4477 10 12 10ZM12 12C13.6569 12 15 10.6569 15 9C15 7.34315 13.6569 6 12 6C10.3431 6 9 7.34315 9 9C9 10.6569 10.3431 12 12 12Z"
      fill={color}
    />
  </svg>
);

Photo.propTypes = propTypes;
Photo.defaultProps = defaultProps;

export default Photo;
