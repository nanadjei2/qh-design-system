import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Cards = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7 2C5.89543 2 5 2.89543 5 4V5C3.89543 5 3 5.89543 3 7V8.26756C2.4022 8.61337 2 9.25972 2 10V18C2 19.1046 2.89543 20 4 20H20C21.1046 20 22 19.1046 22 18V10C22 9.25972 21.5978 8.61337 21 8.26756V7C21 5.89543 20.1046 5 19 5V4C19 2.89543 18.1046 2 17 2H7ZM17 5V4H7V5H17ZM19 7V8H5V7H19ZM20 10H4V18H20V10Z"
      fill={color}
    />
  </svg>
);

Cards.propTypes = propTypes;
Cards.defaultProps = defaultProps;

export default Cards;
