import React from 'react';
import ProgressBar from '../../Components/ProgressBar';
export default {
  title: 'Progress',
  component: ProgressBar
};

const Template = (args) => <ProgressBar {...args} />;

export const Progress = () => {
  return (
    <div>
      <ProgressBar
        className='pb-10'
        progressColor='var(--info)'
        height={10}
        width={10}
        borderRadius={20}
      />
      <ProgressBar
        className='pb-10'
        progressColor='var(--success)'
        height={10}
        width={30}
        borderRadius={20}
      />
      <ProgressBar
        className='pb-10'
        progressColor='var(--warning)'
        height={10}
        width={50}
        borderRadius={20}
      />
      <ProgressBar
        className='pb-10'
        progressColor='var(--danger)'
        height={10}
        width={75}
        borderRadius={20}
      />
    </div>
  );
};

export const ProgressProps = Template.bind({});
ProgressProps.args = {
  width: '',
  height: '',
  progressColor: '',
  borderRadius: '',
  progressHeight: '',
  animate: '',
  animationTiming: '',
  color: ''
};
