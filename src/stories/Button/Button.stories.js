import React from 'react';
import { Fragment } from 'react-is';
import Grid from '../../Components/Grid';
import Button from '../../Components/Button';
import ArrowRight from '../../Icons/ArrowRight';
import ChevronLeft from '../../Icons/ChevronLeft';

export default {
  title: 'Button Variants',
  component: Button
};

const Template = (args) => <Button {...args} />;

export const GenericButton = () => (
  <Fragment>
    <Grid sm='repeat(3, 1fr)'>
      <Button>Button</Button>
      <Button isValid={false}>
        <ChevronLeft color='#fff' />
        <span className='ml-1'>Button</span>
      </Button>
      <Button isSubmitting>Button</Button>

      <Button outline>Button</Button>
      <Button outline isValid={false} color='var(--info)'>
        <ChevronLeft color='var(--info)' />
        <span className='ml-1'>Button</span>
      </Button>
      <Button isSubmitting outline loadingColor='var(--info)'>
        Button
      </Button>

      <Button isValid round>
        <ArrowRight color='#fff' />
      </Button>
      <Button round isValid={false}>
        <ArrowRight color='#fff' />
      </Button>
      <Button isSubmitting round>
        <ArrowRight />
      </Button>

      <Button isValid round outline>
        <ArrowRight color='var(--info)' />
      </Button>
      <Button round outline isValid={false}>
        <ArrowRight color='var(--info)' />
      </Button>
      <Button round outline isSubmitting loadingColor='var(--info)'>
        <ArrowRight />
      </Button>
    </Grid>
    <div className='py-8'>
      <Grid sm='repeat(4, 1fr)'>
        <Button className='btn--sm btn--primary'>Primary</Button>
        <Button className='btn--sm btn--warning'>Warning</Button>
        <Button className='btn--sm btn--success'>Success</Button>
        <Button className='btn--sm btn--danger'>Danger</Button>
      </Grid>
    </div>
  </Fragment>
);

export const LoadingButtonProps = Template.bind({});
LoadingButtonProps.args = {
  isSubmitting: false,
  loadingColor: '',
  isValid: true,
  outline: false,
  round: false
};
