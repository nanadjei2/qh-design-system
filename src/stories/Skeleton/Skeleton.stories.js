import React from 'react';
import Skeleton from '../../Components/Skeleton';
export default {
  title: 'Skeleton',
  component: Skeleton
};

const Template = (args) => <Skeleton {...args} />;

export const Skeletons = () => {
  return <Skeleton type='table' count={7} />;
};

export const SkeletonsProps = Template.bind({});
SkeletonsProps.args = {
  type: 'table',
  count: '7'
};
